﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public float jumpforceRight = 2f;
    public float jumpforceUp = 15f;
    public Rigidbody2D circle;
    public Material circleColor;
    public GameObject platform;
    public Light bgLight;
    public float initialTime;
    public float PlatformPositionX = 3.86f;
    public float PlatformPositionY = -6f;
    public float NewPlatformDistance;
    public int ClickCount = 0;
    public int Score = 0;
    public Text scoreText;

    void Start()
    {
        Time.timeScale = 0; // PAUSE ON START
        circleColor.color = new Color(0.8588235f, 0.3294118f, 0.3294118f);


        for (int a = 0; a < 4; a++)     // add some platforms to begin with
        {
            AddPlatform();
        }
    }

    void Update()
    {
        if (Time.timeScale == 0 && (Input.GetButtonDown("Jump") || Input.GetMouseButtonDown(0)))    // WAIT FOR FIRST CLICK
        {
            Time.timeScale = 1;
            circle.velocity = Vector2.right * jumpforceRight + Vector2.up * jumpforceUp;
        }


        if (Input.GetButtonDown("Jump") || Input.GetMouseButtonDown(0))	// GET USER CLICK
        {
            circle.velocity = Vector2.right * jumpforceRight + Vector2.up * jumpforceUp;
            if (ClickCount == 0)
            {
                circle.velocity = Vector2.right * jumpforceRight + Vector2.up * jumpforceUp;
                ClickCount = 1;
            }
            else
            {
                circle.velocity = Vector2.down * jumpforceUp * 5;
            }
        }

        scoreText.text = Score.ToString();


        if (bgLight.intensity > 0f && circleColor.color == Color.black) // change light 
        {
            bgLight.intensity = bgLight.intensity - 0.001f;
        }
        if (bgLight.intensity < .5f && circleColor.color == new Color(0.8588235f, 0.3294118f, 0.3294118f))
        {
             bgLight.intensity = bgLight.intensity + 0.001f;
        }

    }

    void OnTriggerEnter2D(Collider2D collision)	// COLLISION TRACKING
    {
        if (collision.tag == "Platform")
        {
            Debug.Log("kolizja");
            circle.velocity = Vector2.right * jumpforceRight + Vector2.up * jumpforceUp;
            ClickCount = 0;

            Score++;


            AddPlatform();  // add new platform after scoring


            Debug.Log(Score); // SCORE LOG
            Debug.Log(bgLight.intensity);
        }

        if (collision.tag == "Tower" || collision.tag == "Floor")
        {
            Debug.Log("ded");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        if (collision.tag == "Sky")
        {
            Debug.Log("przestworza");


            if (circleColor.color == new Color(0.8588235f, 0.3294118f, 0.3294118f))
            {
                circleColor.color = Color.black;
            }
            else
            {
                circleColor.color = new Color(0.8588235f, 0.3294118f, 0.3294118f);
            }

        }

    }

    void AddPlatform()
    {
        NewPlatformDistance = 5f + Random.Range(-2.0f, 2.0f);
        PlatformPositionX = PlatformPositionX + NewPlatformDistance;
        Instantiate(platform, new Vector2(PlatformPositionX, PlatformPositionY + Random.Range(-1.5f, 2.0f)), transform.rotation);
    }

}